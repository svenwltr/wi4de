module gitlab.com/svenwltr/wi4de

go 1.13

require (
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/mitchellh/mapstructure v1.1.2
	github.com/pkg/errors v0.8.1
	github.com/rebuy-de/rebuy-go-sdk/v2 v2.0.1-0.20191213090724-bc6ee194bc02
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
)
