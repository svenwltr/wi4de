package dbusutil

import (
	"context"
	"fmt"
	"strings"

	"github.com/godbus/dbus"
	"github.com/godbus/dbus/introspect"
	"github.com/godbus/dbus/prop"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	Wi4deBusName    = "com.gitlab.svenwltr.wi4de"
	Wi4deObjectPath = "/com/gitlab/svenwltr/wi4de"
)

type DBus struct {
	conn *dbus.Conn
}

func NewSession() (*DBus, error) {
	return New(dbus.SessionBus())
}

func NewSystem() (*DBus, error) {
	return New(dbus.SystemBus())
}

func New(conn *dbus.Conn, err error) (*DBus, error) {
	return &DBus{
		conn: conn,
	}, errors.WithStack(err)
}

func (d *DBus) Conn() *dbus.Conn {
	return d.conn
}

func (d *DBus) SetBusName(name string) error {
	reply, err := d.conn.RequestName(name, dbus.NameFlagDoNotQueue)
	if err != nil {
		return errors.Wrap(err, "failed to request bus name")
	}

	if reply != dbus.RequestNameReplyPrimaryOwner {
		return errors.Errorf("bus name already taken")
	}

	return nil
}

func (d *DBus) InterfaceBuilder(name string) *InterfaceBuilder {
	return &InterfaceBuilder{
		name: name,
		conn: d.conn,

		m: map[string]*prop.Prop{},
		i: new(Interface),
	}
}

func (d *DBus) PropertyWatcher() *PropertyWatcher {
	signals := make(chan *dbus.Signal, 10)

	d.conn.Signal(signals)

	return &PropertyWatcher{
		conn:    d.conn,
		signals: signals,

		cache:   map[ObjectPath]map[string]Variant{},
		targets: map[ObjectPath]interface{}{},
		updated: map[ObjectPath]chan struct{}{},
	}
}

type Interface struct {
	props *prop.Properties
}

type InterfaceBuilder struct {
	name string
	conn *dbus.Conn

	m map[string]*prop.Prop
	i *Interface
}

func (b *InterfaceBuilder) CreateProperty(name string, initial interface{}) func(interface{}) error {
	b.m[name] = &prop.Prop{
		Value:    initial,
		Writable: true,
		Emit:     prop.EmitTrue,
	}

	return func(value interface{}) error {
		return errors.WithStack(b.i.props.Set(b.name, name, dbus.MakeVariant(value)))
	}
}

func (b *InterfaceBuilder) Build() (*Interface, error) {
	b.i.props = prop.New(b.conn, Wi4deObjectPath, map[string]map[string]*prop.Prop{
		b.name: b.m,
	})

	nodeSpec := &introspect.Node{
		Name: Wi4deObjectPath,
		Interfaces: []introspect.Interface{
			introspect.IntrospectData,
			prop.IntrospectData,
			{
				Name: b.name,
				//Methods:    introspect.Methods(methods),
				Properties: b.i.props.Introspection(b.name),
			},
		},
	}

	err := b.conn.Export(introspect.NewIntrospectable(nodeSpec), Wi4deObjectPath,
		"org.freedesktop.DBus.Introspectable")
	if err != nil {
		return nil, errors.Wrap(err, "failed to export interface to DBus")
	}

	return b.i, nil
}

type ObjectPath = dbus.ObjectPath
type BusName string
type InterfaceName string

type PropertyWatcher struct {
	conn *dbus.Conn

	cache   map[ObjectPath]map[string]Variant
	targets map[ObjectPath]interface{}
	updated map[ObjectPath]chan struct{}

	signals <-chan *dbus.Signal
}

func (w *PropertyWatcher) Run(ctx context.Context) error {
	for {
		select {
		case <-ctx.Done():
			return nil
		case v := <-w.signals:
			err := w.process(v)
			if err != nil {
				logrus.WithError(err).Error("failed to process signal")
			}
		}
	}
}

func (w *PropertyWatcher) Watch(path ObjectPath, sender BusName, iface InterfaceName, value interface{}, updated *chan struct{}) error {
	rule := strings.Join([]string{
		"type='signal'",
		fmt.Sprintf("path='%s'", path),
		fmt.Sprintf("sender='%s'", sender),
		"interface='org.freedesktop.DBus.Properties'",
		"member='PropertiesChanged'",
	}, ",")
	logrus.WithFields(logrus.Fields{
		"Path":      path,
		"Interface": iface,
		"Sender":    sender,
		"Rule":      rule,
	}).Debug("registering property watcher")

	call := w.conn.BusObject().Call("org.freedesktop.DBus.AddMatch", 0, rule)
	if call.Err != nil {
		return errors.Wrapf(call.Err, "failed to watch property for %s", path)
	}

	// We might need to use a read-write-lock here.
	w.cache[path] = map[string]Variant{}
	w.targets[path] = value

	if updated != nil {
		w.updated[path] = *updated
	}

	err := w.getAll(path, sender, iface)
	if err != nil {
		logrus.WithError(call.Err).Warnf("failed to get initial property for %s", path)
	}

	return nil
}

func (w *PropertyWatcher) getAll(path ObjectPath, sender BusName, iface InterfaceName) error {
	call := w.conn.Object(string(sender), path).
		Call("org.freedesktop.DBus.Properties.GetAll", 0, string(iface))
	if call.Err != nil {
		return errors.Wrap(call.Err, "failed to GetAll properties")
	}

	err := call.Store(w.cache[path])
	if err != nil {
		return errors.Wrap(err, "failed to GetAll properties")
	}

	return errors.WithStack(w.decode(path))
}

func (w *PropertyWatcher) process(s *dbus.Signal) error {
	l := logrus.WithFields(logrus.Fields{
		// Note: We do not actually check, if the signal came from the correct
		// sender. This is, because it only shows the unnamed (unique) BusName
		// and we would need to look it up. This behaviour is fine for now,
		// because we configured the sender in the signal setup.
		"Sender": s.Sender,

		"Path": s.Path,
		"Name": s.Name,
	})
	l.Debug("Received signal")

	if s.Name != `org.freedesktop.DBus.Properties.PropertiesChanged` {
		// Ignoring this would also be fine, but for now we only watch property
		// signals. Therefore, this message might be useful for debugging.
		l.Warn("unknown signal")
		return nil
	}

	var (
		interfaceName string
		changed       map[string]Variant
		invalidated   []string
	)

	err := dbus.Store(s.Body, &interfaceName, &changed, &invalidated)
	if err != nil {
		return errors.Wrap(err, "failed to read signal body")
	}

	l = l.WithField("Interface", interfaceName)

	for name, variant := range changed {
		l.Debugf("Property '%s' changed to '%v'", name, variant)
		w.cache[s.Path][name] = variant
	}

	for _, name := range invalidated {
		l.Debugf("Property '%s' deleted", name)
		delete(w.cache[s.Path], name)
	}

	return errors.WithStack(w.decode(s.Path))
}

func (w *PropertyWatcher) decode(path ObjectPath) error {
	err := DecodeVariant(w.cache[path], w.targets[path])
	if err != nil {
		return errors.Wrap(err, "failed to decode property")
	}

	c, ok := w.updated[path]
	if ok {
		c <- struct{}{}
	}

	return nil
}
