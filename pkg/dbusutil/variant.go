package dbusutil

import (
	"fmt"
	"reflect"

	"github.com/godbus/dbus"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type Variant = dbus.Variant

func DecodeVariant(in interface{}, out interface{}) error {
	var meta mapstructure.Metadata
	config := &mapstructure.DecoderConfig{
		WeaklyTypedInput: true,
		Result:           out,
		Metadata:         &meta,
		TagName:          "dbus",
		ZeroFields:       true,

		DecodeHook: func(from reflect.Type, to reflect.Type, in interface{}) (interface{}, error) {
			switch value := in.(type) {
			case Variant:
				return value.Value(), nil
			default:
				return in, nil
			}
		},
	}

	decoder, err := mapstructure.NewDecoder(config)
	if err != nil {
		return errors.WithStack(err)
	}

	err = errors.WithStack(decoder.Decode(in))
	if err != nil {
		return errors.WithStack(err)
	}

	if len(meta.Unused) > 0 {
		logrus.WithFields(logrus.Fields{
			"Target": fmt.Sprintf("%T", out),
			"Fields": meta.Unused,
		}).Debug("found unsused fields")
	}

	return nil
}
