package dbusutil

import (
	"testing"

	"github.com/godbus/dbus"
	"github.com/rebuy-de/rebuy-go-sdk/v2/pkg/testutil"
	"github.com/sirupsen/logrus"
)

type exampleTarget struct {
	Metadata struct {
		TrackID     string   `dbus:"mpris:trackid"`
		Album       string   `dbus:"xesam:album"`
		AutoRating  float64  `dbus:"xesam:autoRating"`
		DiscNumber  int32    `dbus:"xesam:discNumber"`
		Lenght      uint64   `dbus:"mpris:length"`
		ArtURL      string   `dbus:"mpris:artUrl"`
		TrackNumber int32    `dbus:"xesam:trackNumber"`
		AlbumArtist []string `dbus:"xesam:albumArtist"`
		Title       string   `dbus:"xesam:title"`
	}
}

func TestVariantDecoder(t *testing.T) {
	logrus.SetLevel(logrus.DebugLevel)

	inNative := map[string]interface{}{
		"Metadata": map[string]interface{}{
			"mpris:artUrl":      "https://open.spotify.com/image/927c7b20a8a63cb89696eb7e4447317a342f82b3",
			"mpris:length":      260773000,
			"mpris:trackid":     "spotify:track:5mpUKTdskZea0gStWzeHUZ",
			"xesam:album":       "We Are Not Your Kind",
			"xesam:albumArtist": []string{"Slipknot"},
			"xesam:artist":      []string{"Slipknot"},
			"xesam:autoRating":  0.69,
			"xesam:discNumber":  1,
			"xesam:title":       "Unsainted",
			"xesam:trackNumber": 2,
			"xesam:url":         "https://open.spotify.com/track/5mpUKTdskZea0gStWzeHUZ",
		},
		"PlaybackStatus": "Playing",
	}
	inVariant := dbus.MakeVariant(inNative)

	out := exampleTarget{}
	err := DecodeVariant(inNative, &out)
	if err != nil {
		t.Error(err)
	}
	testutil.AssertGoldenJSON(t, "test-fixtures/unmarshal-golden.json", out)

	out = exampleTarget{}
	err = DecodeVariant(inVariant, &out)
	if err != nil {
		t.Error(err)
	}
	// Both kinds should have the same output, hence the same filename.
	testutil.AssertGoldenJSON(t, "test-fixtures/unmarshal-golden.json", out)

}
