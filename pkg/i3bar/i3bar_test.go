package i3bar

import (
	"testing"

	"github.com/rebuy-de/rebuy-go-sdk/v2/pkg/testutil"
)

func TestRenderI3Bar(t *testing.T) {
	var battery StatusLine
	battery.AddLabel("Battery:")
	battery.Add().SetFullText("54% ⚡")

	var music StatusLine
	music.AddLabel("Music:")
	music.Add().SetFullText("Don't Stop Me Now").SetSeparator(false)
	music.Add().SetFullText("-").SetSeparator(false)
	music.Add().SetFullText("Queen")
	music.Add().SetFullText("⏮️").SetName("music_prev").SetSeparator(false)
	music.Add().SetFullText("⏸️").SetName("music_play").SetSeparator(false)
	music.Add().SetFullText("⏭️").SetName("music_next")

	var result StatusLine
	result.Prepend(&battery)
	result.Prepend(&music)

	testutil.AssertGoldenJSON(t, "test-fixtures/i3bar-golden.json", result)
}
