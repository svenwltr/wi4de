package cmd

import (
	"github.com/rebuy-de/rebuy-go-sdk/v2/pkg/cmdutil"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func NewRootCommand() *cobra.Command {
	var (
		runner = new(DaemonRunner)
	)

	return cmdutil.New(
		"wi4de", "A Go & D-Bus based statusbar for i3 & sway.",
		cmdutil.WithLogVerboseFlag(),
		cmdutil.WithVersionCommand(),
		cmdutil.WithVersionLog(logrus.DebugLevel),

		cmdutil.WithSubCommand(cmdutil.New(
			"statusbar", "Run i3/sway statusbar integration",
			cmdutil.WithRun(RunStatusBar),
		)),

		cmdutil.WithSubCommand(cmdutil.New(
			"daemon", "Run background daemon",
			runner.Bind,
			cmdutil.WithRun(runner.RunDaemon),
		)),
	)
}
