package cmd

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"net"
	"sort"
	"time"

	"gitlab.com/svenwltr/wi4de/pkg/i3bar"
)

type PlayerProperties struct {
	PlaybackStatus string
	Metadata       struct {
		TrackID     string   `dbus:"mpris:trackid"`
		Album       string   `dbus:"xesam:album"`
		AutoRating  float64  `dbus:"xesam:autoRating"`
		DiscNumber  int32    `dbus:"xesam:discNumber"`
		URL         string   `dbus:"xesam:url"`
		Length      uint64   `dbus:"mpris:length"`
		ArtURL      string   `dbus:"mpris:artUrl"`
		Artist      []string `dbus:"xesam:artist"`
		TrackNumber int32    `dbus:"xesam:trackNumber"`
		AlbumArtist []string `dbus:"xesam:albumArtist"`
		Title       string   `dbus:"xesam:title"`
	}
}

type State uint32

const (
	UNKNOWN           State = iota
	CHARGING          State = iota
	DISCHARGING       State = iota
	EMPTY             State = iota
	FULLY_CHARGED     State = iota
	PENDING_CHARGE    State = iota
	PENDING_DISCHARGE State = iota
)

type BatteryProperties struct {
	Energy      float64
	EnergyFull  float64
	Percentage  float64
	TimeToEmpty int64
	TimeToFull  int64
	State       uint32
}

type NetworkProperties struct {
	DeviceType uint32
	Interface  string
	Ip4Address uint32
	State      uint32
}

type StatusBar struct {
	PlayerProperties  PlayerProperties
	BatteryProperties BatteryProperties
	NetworkProperties map[string]*NetworkProperties
}

func (s StatusBar) formatNetworkLine() *i3bar.StatusLine {
	var networkLine i3bar.StatusLine
	networkText := ""

	keys := make([]string, 0, len(s.NetworkProperties))
	for key := range s.NetworkProperties {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	for _, key := range keys {
		networkText = s.NetworkProperties[key].Interface

		if s.NetworkProperties[key].State != 100 {
			segment := networkLine.Add().SetFullText(networkText)
			segment.SetColor("#ba5f5f")
			continue
		}

		ip := make(net.IP, 4)
		binary.LittleEndian.PutUint32(ip, s.NetworkProperties[key].Ip4Address)
		networkText = networkText + " " + ip.String()
		segment := networkLine.Add().SetFullText(networkText)
		segment.SetColor("#5fba7d")

	}
	return &networkLine
}

func (s StatusBar) formatMediaLine() *i3bar.StatusLine {
	var mediaLine i3bar.StatusLine
	if len(s.PlayerProperties.Metadata.Artist) == 0 {
		return &mediaLine
	}

	mediaText := ""

	if s.PlayerProperties.PlaybackStatus == "Playing" {
		mediaText = "⏵ "
	} else {
		mediaText = "⏸ "
	}

	for index, artist := range s.PlayerProperties.Metadata.Artist {
		if index == (len(s.PlayerProperties.Metadata.Artist) - 1) {
			mediaText += artist
		} else {
			mediaText += artist + ", "
		}
	}

	mediaText += " - " + s.PlayerProperties.Metadata.Title
	mediaLine.Add().SetFullText(mediaText)

	return &mediaLine
}

func (s StatusBar) formatBatteryLine() *i3bar.StatusLine {
	var batLine i3bar.StatusLine
	batText := "Battery: "
	additionalText := ""

	switch State(s.BatteryProperties.State) {
	case CHARGING:
		batText = batText + "⚡ "
		hours := s.BatteryProperties.TimeToFull / 3600
		minutes := (s.BatteryProperties.TimeToFull - hours*3600) / 60
		additionalText = fmt.Sprintf(" (%d:%02dh)", hours, minutes)
	case FULLY_CHARGED:
		batText = batText + "🔌 "
	case DISCHARGING:
		batText = batText + "🔋 "
		hours := s.BatteryProperties.TimeToEmpty / 3600
		minutes := (s.BatteryProperties.TimeToEmpty - hours*3600) / 60
		additionalText = fmt.Sprintf(" (%d:%02dh)", hours, minutes)
	case EMPTY:
		batText = batText + "❕ "
	default:
		batText = batText + "❔ "
	}

	batText = batText + fmt.Sprintf("%.f%%%s", s.BatteryProperties.Percentage, additionalText)
	batLine.Add().SetFullText(batText)

	return &batLine
}

func (s StatusBar) MarshalJSON() ([]byte, error) {
	var line i3bar.StatusLine

	line.Add().SetFullText(time.Now().Format(i3bar.DATE_LAYOUT))
	line.Prepend(s.formatBatteryLine())
	line.Prepend(s.formatNetworkLine())
	line.Prepend(s.formatMediaLine())

	return json.Marshal(line.Lines)
}
