package cmd

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/svenwltr/wi4de/pkg/dbusutil"
	"golang.org/x/sync/errgroup"
)

func RunStatusBar(ctx context.Context, cmd *cobra.Command, args []string) {
	fmt.Println(`{ "version": 1, "click_events": true }`)
	fmt.Println(`[`)
	fmt.Println(`[]`)

	for ctx.Err() == nil {
		err := runStatusBar(ctx)
		if err != nil {
			logrus.WithError(err).Error("Statusbar failed.")
			logrus.WithError(err).Debugf("%+v", err)
			fmt.Printf(`,[{"full_text":"error: %v","color":"#ff0000"}]`, err)
			fmt.Println()
		}

		time.Sleep(time.Second)
	}
}

func runStatusBar(ctx context.Context) error {
	sessionBus, err := dbusutil.NewSession()
	if err != nil {
		return errors.Wrap(err, "failed to initialize session DBus")
	}

	updateSignal := make(chan struct{}, 10)

	statusbar := struct {
		StatusLine string `dbus:"StatusLine"`
	}{}

	sessionWatcher := sessionBus.PropertyWatcher()
	err = sessionWatcher.Watch(
		"/com/gitlab/svenwltr/wi4de",
		"com.gitlab.svenwltr.wi4de",
		"com.gitlab.svenwltr.wi4de",
		&statusbar, &updateSignal)
	if err != nil {
		return errors.WithStack(err)
	}

	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		return sessionWatcher.Run(ctx)
	})
	eg.Go(func() error {
		for {
			select {
			case <-ctx.Done():
				return nil
			case <-updateSignal:
			}

			fmt.Println("," + statusbar.StatusLine)
		}
	})

	return errors.WithStack(eg.Wait())
}
