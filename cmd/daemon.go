package cmd

import (
	"context"
	"encoding/json"
	"os"
	"time"

	"github.com/godbus/dbus"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"golang.org/x/sync/errgroup"

	"gitlab.com/svenwltr/wi4de/pkg/dbusutil"
)

type DaemonRunner struct {
	EnableNetworkInfo bool
}

func (runner *DaemonRunner) Bind(cmd *cobra.Command) error {
	cmd.PersistentFlags().BoolVar(
		&runner.EnableNetworkInfo, "enable-network-info", false,
		"Pull network interface information from DBus")
	return nil
}

func (runner *DaemonRunner) RunDaemon(ctx context.Context, cmd *cobra.Command, args []string) {
	err := runner.runDaemon(ctx)
	if err != nil {
		logrus.WithError(err).Error("Daemon execution failed.")
		logrus.WithError(err).Debugf("%+v", err)
		os.Exit(1)
	}
}

func (runner *DaemonRunner) runDaemon(ctx context.Context) error {
	//
	// Setup DBus connections.
	//

	systemBus, err := dbusutil.NewSystem()
	if err != nil {
		return errors.Wrap(err, "failed to initialize system DBus")
	}

	sessionBus, err := dbusutil.NewSession()
	if err != nil {
		return errors.Wrap(err, "failed to initialize session DBus")
	}

	err = sessionBus.SetBusName(dbusutil.Wi4deBusName)
	if err != nil {
		return errors.Wrap(err, "failed to set DBus name")
	}

	//
	// Setup DBus Interface for wi4de.
	//

	builder := sessionBus.InterfaceBuilder("com.gitlab.svenwltr.wi4de")
	setStatusLine := builder.CreateProperty("StatusLine", "[]")
	_, err = builder.Build()
	if err != nil {
		return errors.Wrap(err, "failed to build DBus Interface")
	}

	//
	// Setup StatusBar
	//

	statusBar := new(StatusBar)
	updateSignal := make(chan struct{}, 10)

	//
	// Setup Consumption of other DBus Interfaces.
	//

	sessionWatcher := sessionBus.PropertyWatcher()
	err = sessionWatcher.Watch(
		"/org/mpris/MediaPlayer2",
		"org.mpris.MediaPlayer2.spotify",
		"org.mpris.MediaPlayer2.Player",
		&statusBar.PlayerProperties, &updateSignal)
	if err != nil {
		return errors.WithStack(err)
	}

	systemWatcher := systemBus.PropertyWatcher()
	err = systemWatcher.Watch(
		"/org/freedesktop/UPower/devices/DisplayDevice",
		"org.freedesktop.UPower",
		"org.freedesktop.UPower.Device",
		&statusBar.BatteryProperties, &updateSignal)
	if err != nil {
		return errors.WithStack(err)
	}

	if runner.EnableNetworkInfo {
		networkDevices, err := getNetworkDevices(systemBus)
		if err != nil {
			return errors.WithStack(err)
		}

		statusBar.NetworkProperties = map[string]*NetworkProperties{}
		for _, networkDevice := range networkDevices {
			properties := NetworkProperties{}
			err = systemWatcher.Watch(
				dbus.ObjectPath(networkDevice),
				"org.freedesktop.NetworkManager",
				"org.freedesktop.NetworkManager.Device",
				&properties, &updateSignal)
			statusBar.NetworkProperties[networkDevice] = &properties
			if err != nil {
				return errors.WithStack(err)
			}
		}
	}

	//
	// Start Go routines.
	//

	eg, ctx := errgroup.WithContext(ctx)
	eg.Go(func() error {
		return systemWatcher.Run(ctx)
	})
	eg.Go(func() error {
		return sessionWatcher.Run(ctx)
	})
	eg.Go(func() error {
		t := time.NewTicker(time.Second)
		defer t.Stop()

		for {
			select {
			case <-ctx.Done():
				return nil
			case <-updateSignal:
			case <-t.C:
			}

			bytes, err := json.Marshal(statusBar)
			if err != nil {
				logrus.WithError(err).Error("failed to marshal statusbar")
				continue
			}

			logrus.Info(string(bytes))
			setStatusLine(string(bytes))
		}
	})

	return errors.WithStack(eg.Wait())
}
