package cmd

import (
	"github.com/godbus/dbus"
	"gitlab.com/svenwltr/wi4de/pkg/dbusutil"
)

func getNetworkDevices(systemBus *dbusutil.DBus) ([]string, error) {
	outputArray := []string{}
	dbusObject := systemBus.Conn().Object("org.freedesktop.NetworkManager", "/org/freedesktop/NetworkManager")
	devicesRaw, err := dbusObject.GetProperty("org.freedesktop.NetworkManager.Devices")
	if err != nil {
		return []string{}, err
	}
	devices := devicesRaw.Value().([]dbus.ObjectPath)

	for _, device := range devices {
		dbusObject = systemBus.Conn().Object("org.freedesktop.NetworkManager", device)
		deviceTypeRaw, err := dbusObject.GetProperty("org.freedesktop.NetworkManager.Device.DeviceType")
		if err != nil {
			return []string{}, err
		}
		// only return Ethernet, WiFi & WWAN devices
		// https://developer.gnome.org/NetworkManager/stable/nm-dbus-types.html#NMDeviceType
		if find([]uint32{1, 2, 8}, deviceTypeRaw.Value().(uint32)) {
			outputArray = append(outputArray, string(device))
		}
	}

	return outputArray, nil
}

func find(slice []uint32, val uint32) bool {
	for _, item := range slice {
		if item == val {
			return true
		}
	}
	return false
}
